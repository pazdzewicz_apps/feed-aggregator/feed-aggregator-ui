# Feed Aggregator UI

[![pipeline status](https://gitlab.com/pazdzewicz_apps/feed-aggregator/feed-aggregator-ui/badges/main/pipeline.svg)](https://gitlab.com/pazdzewicz_apps/feed-aggregator/feed-aggregator-ui/-/commits/main)

This Repository contains the RSS feed aggregator frontend web application.

## Setup

### Docker Dev Environment

Start Docker Environment
```
docker-compose up -d
```

Install Dependencies via Composer
```
_bin/npm install
```

## Usage

### Web Access

http://localhost:8080/