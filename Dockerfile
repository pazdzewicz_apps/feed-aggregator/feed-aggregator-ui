FROM node:20

ENV NODE_ENV="production"
ENV API_URL="https://feed-backend.pazdzewicz.de"

RUN mkdir /app

WORKDIR /app

COPY ./src/ /app
COPY ./docker/entrypoint.bash /entrypoint.bash

RUN curl -fsSL https://get.pnpm.io/install.sh | bash - && \
    npm install && \
    npm run build && \
    chmod +x /entrypoint.bash

ENTRYPOINT ["/entrypoint.bash"]