#!/bin/bash
source ~/.bashrc
NODE_ENV=${NODE_ENV:-development}
NPM="$(which npm)"

if [[ ${NODE_ENV} == "development" ]]
then
    ${NPM} install
    ${NPM} run dev
else
    ${NPM} run start
fi