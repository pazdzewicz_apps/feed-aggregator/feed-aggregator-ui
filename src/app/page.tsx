import { allPosts } from "@/.contentlayer/generated"
import { assert } from "console";
//import { allArticles } from "@/.test-data/articles.mjs"
import Link from "next/link"
import internal from "stream"
import dotenv from "dotenv"

type Article = {
  id: number,
  name: string
  content: string
  image: string
  url: string
  feedId: number
  createdAt: number
  modifiedAt: number
}

export default async function Home() {
  dotenv.config();

  let req = await fetch(process.env.API_URL + '/article/index', {
    headers: {
      'X-REQUESTED-WITH': 'XMLHttpRequest'
    }
  })
  let json_data = await req.json();
  let allArticles = json_data.data;
  console.log(json_data.data);

  return (
    <div className="prose dark:prose-invert">
      {allArticles.map((article: Article) => (
        <article key={article.id}>
          <Link href={article.url} target="_blank"><h2>{article.name}</h2></Link>
          {article.content && <p>{article.content}</p>}
        </article>
      ))}
    </div>
  )
}
